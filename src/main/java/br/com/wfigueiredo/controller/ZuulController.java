package br.com.wfigueiredo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * Created by walanem on 20/04/17.
 */
@RestController
public class ZuulController {

    /**
     * Endpoint to simulate an app Timeout
     *
     * @return
     * @throws InterruptedException
     */
    @RequestMapping("/timeout")
    public String timeout() throws InterruptedException {

        TimeUnit.SECONDS.sleep(80);
        return "timeout";
    }
}